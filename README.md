# BERT Android Demo

<img src="./screenshot.png" alt="A screenshot of the app" height="400">

This is an example that shows how to use BERT Text classification models on Android. The models in this repo were trained with the *aim* of detecting child grooming in chats (they are absolutely not ready for real-world use and do not work in practice). But the code could be used to easily inspect the limitations of our models. This is part of [a larger project on early sexual predator detection](https://early-sexual-predator-detection.gitlab.io).

![Warning: Do not use these models in practice. They can not detect real grooming attempts. For more info, read the paper.](do_not_use.svg)

In any case, the code in this repo does not depend on our specific models. If you want to use the code to run your own trained BERT models on Android, independently of our work, you can use this code. Something that is different from this repo to TensorFlow's [Text Classification reference app](https://www.tensorflow.org/lite/inference_with_metadata/task_library/nl_classifier) is that with this code you can also run models with `seq_len`=512.


## Training your own models

See [Text classification with TensorFlow Lite Model Maker](https://www.tensorflow.org/lite/tutorials/model_maker_text_classification) for a step-by-step instruction of building a simple text classification model.

## Android app

Follow the steps below to build and run the sample Android app.

### Requirements

*   Android Studio 3.2 or later. Install instructions can be found on
    [Android Studio](https://developer.android.com/studio/index.html) website.

*   An Android device or an Android emulator and with API level higher than 15.

### Building

*   Open Android Studio, and from the Welcome screen, select `Open an existing
    Android Studio project`.

*   From the Open File or Project window that appears, navigate to and select
    the `eSPD-android-demo/` directory.

*   You may also need to install various platforms and tools according to error
    messages.

*   If it asks you to use Instant Run, click Proceed Without Instant Run.

### Running

*   You need to have an Android device plugged in with developer options enabled
    at this point. See [here](https://developer.android.com/studio/run/device)
    for more details on setting up developer devices.

*   If you already have an Android emulator installed in Android Studio, select
    a virtual device with API level higher than 15.

*   Click `Run` to run the demo app on your Android device.

Note: Part of this README was taken from the [TensorFlow Lite text classification sample
](https://github.com/tensorflow/examples/blob/master/lite/examples/text_classification/android/README.md) repo.
