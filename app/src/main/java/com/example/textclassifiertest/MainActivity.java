package com.example.textclassifiertest;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.tensorflow.lite.support.label.Category;
import org.tensorflow.lite.task.core.TaskJniUtils;
import org.tensorflow.lite.task.text.nlclassifier.BertNLClassifier;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Text Classifier Test";

    private void testLatency(BertNLClassifier classifier) {
        int samples = 20;
        long overallTime = System.currentTimeMillis();
        for (int i = 0; i < samples; i++) {
           long startTime = System.currentTimeMillis();
           String inputText = "measuring inference latency " + i;
           List<Category> results = classifier.classify(inputText);
           Log.v(TAG, "Inference latency " + i + ": " + (System.currentTimeMillis() - startTime) + "ms");

           String textToShow = "Input: " + inputText + "\nOutput:\n";
           Log.v(TAG, "The " + results.size() + " results for input '" + inputText + "' are \n");

           for (int j = 0; j < results.size(); j++) {
               Category result = results.get(j);
               Log.v(TAG, "\tlabel " + result.getLabel() + ": " + result.getScore());
           }
        }
        long stopTime = System.currentTimeMillis();
        long avgLat = (stopTime - overallTime) / samples;
        Log.v(TAG, "Average Inference latency based on " + samples + " inferences: " + avgLat + " ms");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String bert_base = "2020-11-21_23-11-03__bert_classifier_on_PANC_with_seq-len-512/quantized/model.tflite";
        String mobile_bert = "2020-11-30_17-08-16__mobilebert_classifier_on_PANC_with_seq-len-512/quantized/model.tflite";
        Context context = getApplicationContext();

        try {
            int seq_len = 512;
            BertNLClassifier.BertNLClassifierOptions options =
                   BertNLClassifier.BertNLClassifierOptions.builder().setMaxSeqLen(seq_len).build();

            Log.v(TAG, "Options: " + options.toString());

            // BERT-base, classification takes about 5600ms
            long start = System.currentTimeMillis();
            ByteBuffer modelBuffer_base = TaskJniUtils.loadMappedFile(context, bert_base);
            BertNLClassifier bert_base_classifier = BertNLClassifier.createFromBufferAndOptions(modelBuffer_base, options);
            long finish = System.currentTimeMillis();
            Log.v(TAG,"Initializing bert_base took " + ((int) Math.ceil(finish-start)) + "ms");

            // MobileBERT, classificaiton takes about 2000ms
            long start2 = System.currentTimeMillis();
            ByteBuffer modelBuffer_mobile = TaskJniUtils.loadMappedFile(context, mobile_bert);
            BertNLClassifier mobile_bert_classifier = BertNLClassifier.createFromBufferAndOptions(modelBuffer_mobile, options);
            long finish2 = System.currentTimeMillis();
            Log.v(TAG,"Initializing MobileBERT took " + ((int) Math.ceil(finish2-start2)) + "ms");           


//             old version:
//            BertNLClassifier classifier = BertNLClassifier.createFromFile(context, pathToModel);

//            testLatency(mobile_bert_classifier);
//            testLatency(bert_base_classifier);

            Button classifyButton = findViewById(R.id.button_classify);
            EditText input = findViewById(R.id.classification_input);
            TextView output = findViewById(R.id.classification_output);
            output.setMovementMethod(new ScrollingMovementMethod()); // make scrollable

            RadioGroup classifier_radio = findViewById(R.id.classifier_radio);

            classifyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.v(TAG, "classifying…");
                    String inputText = input.getText().toString();
                    long start = System.currentTimeMillis();
                    RadioButton classifier_radio_button = findViewById(classifier_radio.getCheckedRadioButtonId());
                    Log.v(TAG, classifier_radio_button.getText().toString());
                    BertNLClassifier classifier =
                            classifier_radio_button.getText().equals("BERT Base") ?
                                    bert_base_classifier : mobile_bert_classifier;
                    List<Category> results = classifier.classify(inputText);
                    long finish = System.currentTimeMillis();

                    String textToShow = "Input: " + inputText + "\nClass probabilities:";

                    for (int j = 0; j < results.size(); j++) {
                        Category result = results.get(j);
                        textToShow += "\n\t" + result.getLabel() + ": " + String.format("%.4f", result.getScore());
                    }
                    textToShow += "\n\n Took " + ((int) Math.ceil(finish-start)) + "ms ";
                    output.setText(textToShow + "\n————————————————\n" + output.getText());
                    Log.v(TAG, textToShow);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
